local wezterm = require 'wezterm'
local config = wezterm.config_builder()
config.color_scheme = 'ayu'

--config.font = wezterm.font 'Cozette'
--config.font_size = 12
config.font = wezterm.font 'Departure Mono'
config.font_size = 8
config.freetype_load_target = 'Light'
config.freetype_render_target = 'Mono'
config.enable_tab_bar = false
config.window_padding = {
  left = 0,
  right = 0,
  top = 0,
  bottom = 0,
}
return config
