#!/bin/bash
# autostart.sh - start my shit when i wake up
# thanks https://wiki.archlinux.org/index.php/awesome#Autorun_programs

set -eu

run() {
  if ! pgrep "$1" ;
  then
    notify-send "autostart: $1 start"
    "$@" &
  else
    notify-send "autostart: $1 already up"
  fi
}

if [ -f ~/.config/til/autostart_env.sh ]; then
  echo 'source from env'
  source $HOME/.config/til/autostart_env.sh
fi

notify-send "autostart!"

# woo us layout babey
setxkbmap -layout us

# redshift is important for quality of life
run redshift

# other quality of life things
run gnome-system-monitor
run conky
run syncthing-gtk
run pavucontrol

# epic meme sending
run flameshot

# real
if [ "$AUTOSTART_NICOTINE" = "1" ]; then
  run nicotine
fi
if [ "$AUTOSTART_BARRIER" = "1" ]; then
  run barrier
fi

if [ -f ~/.config/til/custom_autostart.sh ]; then
  echo 'running custom autostart'
  ~/.config/til/custom_autostart.sh
fi
