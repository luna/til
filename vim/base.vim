filetype plugin indent on

syntax on

set autoindent
set expandtab
set softtabstop =4
set shiftwidth  =4
set shiftround
set ttimeoutlen=50
" set paste

set backspace   =indent,eol,start
set hidden
set laststatus  =2
set display     =lastline

set showmode
set showcmd

set incsearch
set hlsearch

set lazyredraw

set splitbelow
set splitright

set cursorline
set wrapscan
set report      =0
set synmaxcol   =0

" this sets nice multibyte representation
" for tabs and all
set list
if has('multi_byte') && &encoding ==# 'utf-8'
  let &listchars = 'tab:▸ ,extends:❯,precedes:❮,nbsp:±'
else
  let &listchars = 'tab:> ,extends:>,precedes:<,nbsp:.'
endif

" The fish shell is not very compatible to other shells and unexpectedly
" breaks things that use 'shell'.
if &shell =~# 'fish$'
  set shell=/bin/bash
endif

set mouse=a
set colorcolumn=80

" prevent arrows being used.
" at least when i'm not on insert mode
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" life saver
cnoreabbrev Vsplit vsplit
cnoreabbrev Split split
cnoreabbrev W w
cnoreabbrev Wq wq
cnoreabbrev Qa qa

" file specific

autocmd BufNewFile,BufRead *.go setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
autocmd BufNewFile,BufRead *.v setlocal tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab
autocmd BufNewFile,BufRead *.cr setlocal tabstop=2 softtabstop=2 shiftwidth=2
