local gears = require("gears")
local config_home = gears.filesystem.get_xdg_config_home()

local file, err = io.open(config_home .. "/til.conf", "r")
if not file then
    debug_val('failed to open til.conf')
    debug_val(err)
    return err
end

local config = {}

for line in file:lines() do
    for key, value in line:gmatch("(%S+) (%S+)") do
        config[key] = value
    end
end

file:close()

debug_val("loaded config")
return config
