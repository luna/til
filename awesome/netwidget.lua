local mod = {}

local wibox = require("wibox")
local gears = require("gears")
local platform = require("platform")

-- This is a direct port of the network widget from Vicious made by Luna.
--
-- Back on the AwesomeWM v3 to v4 transition, I couldn't find Vicious being
-- ported to v4. I did it myself. Was a fun night.

-- Variable definitions
local unit = {
    ["b"] = 1, ["kb"] = 1024,
    ["mb"] = 1024^2, ["gb"] = 1024^3
}

-- format our values to a specific unit
-- returns nil on invalid units
local function unit_format(val, unit_str)
    if unit[unit_str] == nil then return nil end
    return string.format("%.2f", val / unit[unit_str])
end

function mod.CreateWidget()
    local netwidget = wibox.widget.textbox()
    netwidget:set_text(" NETWORK | ")

    local netstate_data = {}

    local interface = platform.config.network_interface
    assert(interface, "config 'network_interface' is required")

    local netwidget_timer = gears.timer({ timeout = 1.35 })
    netwidget_timer:connect_signal("timeout",
        function()
            platform.network.state_tick(netstate_data)
            local data = netstate_data[interface]
            assert(data, "interface "..interface.." not found")

            netwidget:set_text(string.format(" ▲%s▲ ▼%s▼ | ",
                unit_format(data.up, "kb"),
                unit_format(data.down, "kb")))
        end
    )
    netwidget_timer:start()

    return {netwidget}
end

return mod
