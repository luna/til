
local mod = {}

local wibox = require("wibox")
local gears = require("gears")
local platform = require("platform")
local utils = require("utils")
local awful = require("awful")
local naughty = require("naughty")

function mod.CreateWidget()
    local tempwidget = wibox.widget.textbox()
    tempwidget:set_text("")

    -- local interface = platform.config.temp_widget
    -- assert(interface, "config 'network_interface' is required")

    local wanted_temps = {}
    local thresholds = {}
    for config_key, sensor_system_key in pairs(platform.config) do
        for device_type in config_key:gmatch("temp_sensor_(%S+)") do
            wanted_temps[device_type] = sensor_system_key
        end
        for device_type in config_key:gmatch("temp_threshold_(%S+)") do
            thresholds[device_type] = tonumber(sensor_system_key)
        end
    end

    local json = require("json")

    local temp_bumps = {}
    for device_type, _ in pairs(wanted_temps) do
        temp_bumps[device_type] = 0
    end

    local tempwidget_timer = gears.timer({ timeout = 2 })
    tempwidget_timer:connect_signal("timeout",
        utils.pcall_naughty_timer(tempwidget_timer, function()
            local result = platform.temperatures.fetch_all()
            utils.result_async(result, function(stdout)
                local temperatures = platform.temperatures.fetch_all_callback(stdout)

                local latest_temps = {}

                for device_type, sensor_system_key in pairs(wanted_temps) do
                    local current_temp = 0
                    for property_key, temperature in pairs(temperatures[sensor_system_key]) do
                        if temperature > current_temp then
                            current_temp = temperature
                        end
                    end

                    latest_temps[device_type] = current_temp

                    local threshold_temp = thresholds[device_type]
                    if current_temp >= threshold_temp then
                        temp_bumps[device_type] = temp_bumps[device_type] + 1
                    else
                        temp_bumps[device_type] = 0
                    end
                end

                for device_type, bumps in pairs(temp_bumps) do
                    if bumps > 2 then
                        naughty.notify({
                            preset = naughty.config.presets.critical,
                            title = string.format("TEMPERATURE CRITICAL on device %s", device_type),
                            text = string.format("threshold %.2f, current %.2f", thresholds[device_type], latest_temps[device_type]),
                            timeout = 4,
                        })

                        temp_bumps[device_type] = 0
                    end
                end
            end)

        end)
    )
    tempwidget_timer:start()

    return {tempwidget}
end

return mod
