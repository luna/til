local mod = {}
local conky = nil

function fetch_conky(awful)
    if conky and conky.valid then
        return conky
    end

    conky = awful.client.iterate(function(c) return c.class == "Conky" end)()
    return conky or nil
end

local function set_conky_ontop(awful, value)
    local win = fetch_conky(awful)
    if win == nil then return end
    win.ontop = value
end

function mod.raise(awful)
    set_conky_ontop(awful, true)
end

function mod.lower(awful)
    set_conky_ontop(awful, false)
end

function mod.toggle(awful)
    local conky_window = fetch_conky(awful)

    if conky_window == nil then
        debug_err("conky window not found")
        return
    end

    conky_window.ontop = not conky_window.ontop
end

function mod.setup(awful)
    mod.lower(awful)
end

return mod
