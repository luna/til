-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup").widget

local main_screen = 1
VR = false

naughty.config.defaults.icon_size = 64

naughty.config.presets.critical = {
    bg = "#ff0000",
    fg = "#000000",
    timeout = 0,
}

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = tostring(err) })
        in_error = false
    end)
end
-- }}}

-- custom debug function
function debug_val(v)
    naughty.notify({ title      = "debug"
                    , text       = tostring(v)
                    , timeout    = 5
                    , position   = "top_right"
                    , fg         = beautiful.fg_focus
                    , bg         = beautiful.bg_focus
                    })
end

function debug_err(v)
    naughty.notify({ title      = "debug"
                    , text       = tostring(v)
                    , timeout    = 0
                    , position   = "top_right"
                    , fg         = "#000000"
                    , bg         = "#ff0000"
                    })
end

local homedir = os.getenv("HOME")
local tildir = homedir.."/til"

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
--
-- this is a custom theme for me.
local theme = require("theme")
if not beautiful.init(theme) then
    beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
end

-- make the platform module load so we have config for the rest of rc to use
local config = require("config")
local platform = require("platform")

-- This is used later as the default terminal and editor to run.
local terminal = "xterm"
if platform ~= nil then
    terminal = platform.config.terminal
end
local editor = os.getenv("EDITOR") or "nano"
local editor_cmd = terminal .. " -e " .. editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
local modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    awful.layout.suit.tile.top,
    --awful.layout.suit.fair,
    awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    --awful.layout.suit.magnifier,
    awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}

-- {{{ Helper functions
local function client_menu_toggle_fn()
    local instance = nil

    return function ()
        if instance and instance.wibox.visible then
            instance:hide()
            instance = nil
        else
            instance = awful.menu.clients({ theme = { width = 250 } })
        end
    end
end
-- }}}

-- {{{ Menu
-- Create a launcher widget and a main menu
local myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end},
   { "manual", terminal .. " -e man awesome" },
   { "edit config", editor_cmd .. " " .. awesome.conffile },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end}
}

local mymainmenu = awful.menu({
    items = {
        { "awesome", myawesomemenu, beautiful.awesome_icon },
        { "open terminal", terminal }
    }
})

local mylauncher = awful.widget.launcher({
    image = beautiful.awesome_icon,
    menu = mymainmenu
})

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}



-- Keyboard map indicator and switcher
local mykeyboardlayout = awful.widget.keyboardlayout()

-- {{{ Wibar

-- load custom widgets and modules
local right_widgets = {
    layout = wibox.layout.fixed.horizontal,
    mykeyboardlayout,
    wibox.widget.systray(),
}

local function widget_table_extend(original, other)
    if platform == nil then
        debug_err("did not load widget due to no platform")
    else
        for _, value in ipairs(other) do
            table.insert(original, value)
        end
    end
end

if platform.config.temp_widget == "yes" then
    local cputemp_module = require("cputemp")
    widget_table_extend(right_widgets, cputemp_module.CreateWidget())
end

local stats_module = require("stats")
widget_table_extend(right_widgets, stats_module.CreateWidget())

local wifi_strength_module = require("wifi_strength")
if not platform.config.wifi_disabled then
    widget_table_extend(right_widgets, wifi_strength_module.CreateWidget())
end

local battery_module = require("battery")
if not platform.config.battery_disabled then
    widget_table_extend(right_widgets, battery_module.CreateWidget())
end

local netwidget_module = require("netwidget")
widget_table_extend(right_widgets, netwidget_module.CreateWidget())

local volume_module = require("volume")
widget_table_extend(right_widgets, volume_module.CreateWidget())

local textclock_module = require("textclock")
widget_table_extend(right_widgets, textclock_module.CreateWidget())

local conky = require("conky")
conky.setup(awful)

-- Create a wibox for each screen and add it
local taglist_buttons = awful.util.table.join(
                    awful.button({ }, 1, function(t) t:view_only() end),
                    awful.button({ modkey }, 1, function(t)
                                              if client.focus then
                                                  client.focus:move_to_tag(t)
                                              end
                                          end),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, function(t)
                                              if client.focus then
                                                  client.focus:toggle_tag(t)
                                              end
                                          end),
                    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
                    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
                )

local tasklist_buttons = awful.util.table.join(
                     awful.button({ }, 1, function (c)
                                              if c == client.focus then
                                                  c.minimized = true
                                              else
                                                  -- Without this, the following
                                                  -- :isvisible() makes no sense
                                                  c.minimized = false
                                                  if not c:isvisible() and c.first_tag then
                                                      c.first_tag:view_only()
                                                  end
                                                  -- This will also un-minimize
                                                  -- the client, if needed
                                                  client.focus = c
                                                  c:raise()
                                              end
                                          end),
                     awful.button({ }, 3, client_menu_toggle_fn()),
                     awful.button({ }, 4, function ()
                                              awful.client.focus.byidx(1)
                                          end),
                     awful.button({ }, 5, function ()
                                              awful.client.focus.byidx(-1)
                                          end))

if platform ~= nil then
    beautiful.wallpaper = function(s)
        local specific_screen_property = 'wallpaper' .. tostring(s.index)
        local maybe_screen_wallpaper = platform.config[specific_screen_property]
        if maybe_screen_wallpaper then
            return maybe_screen_wallpaper
        else
            return platform.config.wallpaper
        end
    end
end

local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
    awful.tag({ "m", "www", "work", "ext", "mon", "media", "etc", "game", "dis" }, s, awful.layout.layouts[1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox

    -- instead of copying the table of widgets, we keep the references to the
    -- original widgets, since they contain the primary state we should
    -- draw data from
    local screen_widgets = {}
    for k, v in pairs(right_widgets) do
        screen_widgets[k] = v
    end

    -- though, each screen has its own layoutbox, so we need to account for
    -- that after creating the list of screen specific widgets
    table.insert(screen_widgets, s.mylayoutbox)

    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        screen_widgets
    }
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    --awful.key({ modkey, "Shift"   }, "q", awesome.quit,
    --          {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"}),

    awful.key({}, "Pause", function()
        conky.toggle(awful)
    end)
)

clientkeys = awful.util.table.join(
    awful.key({ modkey,           }, "f",
        function (c)
            c.fullscreen = not c.fullscreen
            c:raise()
        end,
        {description = "toggle fullscreen", group = "client"}),
    awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end,
              {description = "close", group = "client"}),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ,
              {description = "toggle floating", group = "client"}),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end,
              {description = "move to master", group = "client"}),
    awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
              {description = "move to screen", group = "client"}),
    awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end,
              {description = "toggle keep on top", group = "client"}),
    awful.key({ modkey,           }, "n",
        function (c)
            -- The client currently has the input focus, so it cannot be
            -- minimized, since minimized clients can't have the focus.
            c.minimized = true
        end ,
        {description = "minimize", group = "client"}),
    awful.key({ modkey,           }, "m",
        function (c)
            c.maximized = not c.maximized
            c:raise()
        end ,
        {description = "maximize", group = "client"}),

    awful.key({modkey}, "d",
        function()
            awful.spawn.easy_async(
                "rofi -combi-modi window,drun -show combi -modi combi",
                function(stdout, stderr, _, exit_code)
                    if exit_code ~= 0 then
                        naughty.notify({
                            preset = naughty.config.presets.critical,
                            title = "rofi fail",
                            text = stdout..stderr,
                            timeout = 3,
                        })
                    end
                end
            )
        end),

    awful.key({}, "Print",
        function()
            awful.spawn.easy_async(
                homedir .. "/git/cute-scripts/bin/screenie",
                function(stdout, stderr, _, exit_code)
                    if exit_code ~= 0 then
                        naughty.notify({
                            preset = naughty.config.presets.critical,
                            title = "screenie fail",
                            text = stdout..stderr,
                            timeout = 3,
                        })
                    end
                end
            )
        end),

    awful.key({modkey, "Shift"}, "u",
        function()
            awful.spawn.easy_async(
                homedir .. "/bin/uuidpaste",
                function(stdout, stderr, _, exit_code)
                    if exit_code ~= 0 then
                        naughty.notify({
                            preset = naughty.config.presets.critical,
                            title = "uuidpaste fail",
                            text = stdout..stderr,
                            timeout = 10,
                        })
                    end
                end
            )
        end)
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
    globalkeys = awful.util.table.join(globalkeys,
        -- View tag only.
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = awful.screen.focused()
                        local tag = screen.tags[i]
                        if tag then
                           tag:view_only()
                        end
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Toggle tag display.
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = awful.screen.focused()
                      local tag = screen.tags[i]
                      if tag then
                         awful.tag.viewtoggle(tag)
                      end
                  end,
                  {description = "toggle tag #" .. i, group = "tag"}),
        -- Move client to tag.
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #"..i, group = "tag"}),
        -- Toggle tag on focused client.
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus then
                          local tag = client.focus.screen.tags[i]
                          if tag then
                              client.focus:toggle_tag(tag)
                          end
                      end
                  end,
                  {description = "toggle focused client on tag #" .. i, group = "tag"})
    )
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = clientkeys,
                     buttons = clientbuttons,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen
     }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
        },
        class = {
          "Arandr",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Wpa_gui",
          "pinentry",
          "veromix",
          "xtightvncviewer"},

        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = true }
    },

    -- Set Firefox to always map on the tag named "2" on screen 1.
    { rule = { class = "Firefox" },
       properties = { screen = main_screen, tag = "www" } }, --tag 4("www") screen 1
    { rule = { class = "Pcmanfm" },
      properties = { screen = main_screen, tag = "ext" } }, --tag 4("term") screen 1
    { rule = { class = "Conky" },
        properties = {
            floating = true,
            sticky = true,
            ontop = false,
            focusable = false
    } },
    { rule = { class = "org.gajim.Gajim" },
        properties = {
            urgent = false
    } }

}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    if awesome.startup and
      not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
end)

-- Function to calculate the luminance of an RGB color
local function rgb_luminance(r, g, b)
    r = r / 255
    g = g / 255
    b = b / 255

    local function adjustColorComponent(component)
        if component <= 0.03928 then
            return component / 12.92
        else
            return math.pow((component + 0.055) / 1.055, 2.4)
        end
    end

    r = adjustColorComponent(r)
    g = adjustColorComponent(g)
    b = adjustColorComponent(b)

    return 0.2126 * r + 0.7152 * g + 0.0722 * b
end

-- Function to generate a high-contrast background color for a given RGB foreground color
local function rgb_background(fg_R, fg_G, fg_B)
    -- Calculate the luminance of the foreground color
    local luma = rgb_luminance(fg_R, fg_G, fg_B)

    -- Determine a suitable background color based on luminance
    local bg_R, bg_G, bg_B

    -- Define a constant for the contrast factor (adjust as needed)
    local contrastFactor = 0.4

    if luma > 0.5 then
        -- If the foreground color is light, make the background darker
        bg_R = fg_R - 255 * contrastFactor
        bg_G = fg_G - 255 * contrastFactor
        bg_B = fg_B - 255 * contrastFactor
    else
        -- If the foreground color is dark, make the background lighter
        bg_R = fg_R + 255 * contrastFactor
        bg_G = fg_G + 255 * contrastFactor
        bg_B = fg_B + 255 * contrastFactor
    end

    -- Ensure the background color components are within the valid range (0-255)
    bg_R = math.max(0, math.min(255, bg_R))
    bg_G = math.max(0, math.min(255, bg_G))
    bg_B = math.max(0, math.min(255, bg_B))

    return {bg_R, bg_G, bg_B}
end

local function rgb_darker(r, g, b)
    -- Define a constant for the contrast factor (adjust as needed)
    local contrastFactor = 0.2

    -- If the foreground color is light, make the background darker
    r = math.max(r - 255 * contrastFactor, 0)
    g = math.max(g - 255 * contrastFactor, 0)
    b = math.max(b - 255 * contrastFactor, 0)
    return {r,g,b}
end


local function rgb_from_string(input)
    local hash = 0
    for i = 1, #input do
        local char = string.byte(input, i)
        hash = (hash * 31 + char) % 256
    end

    local r = hash
    local g = (hash + 85) % 256
    local b = (hash + 170) % 256

    return {r, g, b}
end

local function rgb_to_hex(packed)
    return string.format("#%02x%02x%02x", table.unpack(packed))
end

local SSH_REGEX = "@(.-):"
local function maybe_set_titlebar_from_name(c)
    if c.name == nil then
        return
    end
    local match = string.match(c.name, SSH_REGEX)
    if match then
        local fg_color = rgb_from_string(match)
        local fg_unfocused = rgb_darker(table.unpack(fg_color))
        local bg_color = rgb_background(table.unpack(fg_color))
        local bg_unfocused = rgb_darker(table.unpack(bg_color))
        awful.titlebar(c, {
            bg_focus = rgb_to_hex(bg_color),
            bg_normal = rgb_to_hex(bg_unfocused),
            fg_focus = rgb_to_hex(fg_color),
            fg_normal = rgb_to_hex(fg_unfocused),
        })
    else
        -- reset
        awful.titlebar(c)
    end
end

client.connect_signal("property::name", function(c)
    maybe_set_titlebar_from_name(c)
end)

client.connect_signal("property::urgent", function(c)
    if c.name == nil then
        return
    end
    if not c.urgent then
        return
    end
    -- gajim has this for xprop WM_CLASS
    -- WM_CLASS(STRING) = "org.gajim.Gajim", "Launch.py"
    if c.class == "Launch.py" then
        c.urgent = false
    end
    if c.class == "launch.py" then
        c.urgent = false
    end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
    -- buttons for the titlebar
    local buttons = awful.util.table.join(
        awful.button({ }, 1, function()
            client.focus = c
            c:raise()
            awful.mouse.client.move(c)
        end),
        awful.button({ }, 3, function()
            client.focus = c
            c:raise()
            awful.mouse.client.resize(c)
        end)
    )

    awful.titlebar(c) : setup {
        { -- Left
            awful.titlebar.widget.iconwidget(c),
            buttons = buttons,
            layout  = wibox.layout.fixed.horizontal
        },
        { -- Middle
            { -- Title
                align  = "center",
                widget = awful.titlebar.widget.titlewidget(c)
            },
            buttons = buttons,
            layout  = wibox.layout.flex.horizontal
        },
        { -- Right
            awful.titlebar.widget.maximizedbutton(c),
            awful.titlebar.widget.stickybutton   (c),
            awful.titlebar.widget.ontopbutton    (c),
            awful.titlebar.widget.closebutton    (c),
            layout = wibox.layout.fixed.horizontal()
        },
        layout = wibox.layout.align.horizontal
    }

    maybe_set_titlebar_from_name(c)
end)

-- Enable sloppy focus, so that focus follows mouse.
if not VR then
    client.connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)
end

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- autorun programs
-- {{{

awful.spawn.with_line_callback(tildir.."/bin/autostart.sh", {
    stdout = function(line)
        naughty.notify({
            title = "autostart",
            text = line,
            timeout = 10,
            position = "top_right"
        })
    end,
    stderr = function(line)
        naughty.notify({
            title = "autostart (stderr)",
            text = line,
            timeout = 10,
            position = "top_right"
        })
    end,
    exit = function(reason, code)
        local data = {
            title = "autostart exited",
            text = reason .. ' ' .. code,
            timeout = 4,
        }

        if reason ~= "exit" and code ~= 0 then
            data.preset = naughty.config.presets.critical
        end

        naughty.notify(data)
    end
})

-- }}}
