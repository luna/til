local mod = {}

local naughty = require("naughty")
local awful = require("awful")

-- Returns a function that wraps the given function in pcall() and emits a
-- naughty error if the pcall() fails.
function mod.pcall_naughty(func)
    assert(func, "function required")

    local function wrapped()
        local ok, err = pcall(func)
        if not ok then
            naughty.notify({
                preset = naughty.config.presets.critical,
                title = "an error happened",
                text = err,
            })
        end

        return ok, err
    end

    return wrapped
end

-- Returns a wrapped function that calls `func` in pcall().
-- If the call errors, the given `timer` will have its "timeout" signal
-- disconnected.
function mod.pcall_naughty_timer(timer, func)
    local wrapped = mod.pcall_naughty(func)

    local function wrapped_2()
        local ok, err = wrapped()
        if not ok then
            timer:disconnect_signal("timeout", wrapped_2)
        end

        return ok, err
    end

    return wrapped_2
end

function mod.result_async(result, callback, params)
    params = params or {}
    assert(type(result) == 'table', "result is not table ("..tostring(result)..")")
    assert(type(callback) == 'function', "callback is not function ("..tostring(callback)..")")

    -- there are no ways to read files by default (io:open is SYNCHRONOUS)
    -- so we asyncily spawn 'cat' and boom, we have our file
    --
    -- cat(1) is defined by POSIX so its okay :)
    if result.type == 'file' then
        return mod.result_async({type='cmd', command=string.format('cat "%s"', result.filepath)}, callback)
    elseif result.type == 'cmd' or result.type == 'shell' then
        local func = nil
        if result.type == 'cmd' then
            func = awful.spawn.easy_async
        else
            func = awful.spawn.easy_async_with_shell
        end
        local pid = func(result.command, function(stdout, stderr, exitreason, exitcode)
            mod.pcall_naughty(function()
                if not params.errors then
                    assert(exitcode == 0, string.format("command(%s) failed(exit=%d): reason=%s;\nstderr=%s", result.command, exitcode, exitreason, stderr))
                    callback(stdout)
                else
                    callback(exitcode, stdout)
                end
            end)()
        end)

        if type(pid) ~= 'number' then
            debug_err(string.format("command(%s) failed: %s", result.command, pid))
        end
    elseif result.type == 'data' then
        mod.pcall_naughty(function()
            callback(result.data)
        end)()

    -- some platforms might want to resolve multiple futures at once
    -- (such as linux's battery function, that requires reading two files)
    -- to know the actual battery charge
    --
    -- to do this, we recursively iterate through the given futures,
    -- resolving one by one, and by the end of the future array, call
    -- the original callback with all the data we gathered
    --
    -- this looks like python's asyncio.gather or js' promise.all
    --  because it is lol
    elseif result.type == 'multiple' then
        local futures = result.futures
        local gathered_data = {}

        local index = 1

        local function callback_multiple_item(single_data)
            table.insert(gathered_data, single_data)
            index = index + 1
            local future = futures[index]
            if future == nil then
                mod.result_async({type='data', data=gathered_data}, callback)
            else
                mod.result_async(future, callback_multiple_item)
            end
        end
        mod.result_async(futures[1], callback_multiple_item)
    else
        assert(false, "unknown async future type"..result.type)
    end
end

return mod
