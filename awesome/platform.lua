-- Load the preffered platform module

local config = require("config")
local wanted_platform = config["platform"]
local PLATFORMS = {
    ["linux"]=require("platform/linux")
}

local platform_module = PLATFORMS[wanted_platform]

if platform_module == nil then
    debug_err("failed to load platform: "..wanted_platform)
else
    platform_module["config"] = config
    debug_val("loaded platform: " .. wanted_platform)
end

return platform_module
