local mod = {}

local platform = require("platform")
local utils = require("utils")

local wibox = require("wibox")
local awful = require("awful")
local naughty = require("naughty")
local gears = require("gears")

function mod.CreateWidget()
    local batterywidget = wibox.widget.textbox()
    local batterywidget_tip = awful.tooltip({ objects = { batterywidget }})
    batterywidget:set_text(" BATTERY | ")

    local batterywidget_timer = gears.timer({ timeout = 30 })
    batterywidget_timer:connect_signal("timeout",
        utils.pcall_naughty_timer(batterywidget_timer, function()
            local result = platform.battery()

            utils.result_async(result, function(data)
                local widget_results = platform.battery_callback(data)
                local tip, percentage = widget_results.tip, widget_results.percentage

                batterywidget_tip:set_text(tip)
                batterywidget:set_text(string.format(" b%s%% | ", percentage))

                local percentage_num = tonumber(percentage)
                assert(percentage_num, "given percentage is not a number("..percentage..")")
                if percentage_num <= 15 then
                    naughty.notify({
                        preset = naughty.config.presets.critical,
                        title = "BATTERY CRITICAL",
                        text = percentage..'% left',
                        timeout = 4,
                    })
                end
            end)
        end)
    )

    batterywidget_timer:start()

    return {batterywidget}
end

return mod
