local mod = {}

local platform = require("platform")
local utils = require("utils")
local wibox = require("wibox")
local gears = require("gears")

function mod.CreateWidget()
    local ramwidget = wibox.widget.textbox()
    local meminfo_timer = gears.timer({ timeout = 0.7 })
    meminfo_timer:connect_signal("timeout",
        utils.pcall_naughty_timer(meminfo_timer, function()
            local result = platform.ram_usage()
            utils.result_async(result, function(stdout)
                local text = string.format("| m%.0f%% ", platform.ram_usage_callback(stdout))
                ramwidget:set_text(text)
            end)
        end)
    )
    meminfo_timer:start()

    local cpuwidget = wibox.widget.textbox()
    local cpuinfo_timer = gears.timer({ timeout = 0.8 })
    cpuinfo_timer:connect_signal("timeout",
        utils.pcall_naughty_timer(cpuinfo_timer, function()
            local result = platform.cpu_usage()
            utils.result_async(result, function(stdout)
                local text = string.format(" c%02d%% ", math.floor(platform.cpu_usage_callback(stdout)))
                cpuwidget:set_text(text)
            end)
        end)
    )
    cpuinfo_timer:start()

    --local gpuwidget = wibox.widget.textbox()
    --local gpuinfo_timer = gears.timer({ timeout = 3 })
    --gpuinfo_timer:connect_signal("timeout",
    --    utils.pcall_naughty_timer(gpuinfo_timer, function()
    --        local result = platform.gpu_usage()
    --        utils.result_async(result, function(stdout)
    --            local usage= platform.gpu_usage_callback(stdout)
    --            local text = string.format(" g%02d%% ", math.floor(usage))
    --            gpuwidget:set_text(text)
    --        end)
    --    end)
    --)
    --gpuinfo_timer:start()

    return {ramwidget, cpuwidget}--, gpuwidget}
end

return mod

