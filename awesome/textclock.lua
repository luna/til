local mod = {}

local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")
local utils = require("utils")
local platform = require("platform")

function mod.CreateWidget()
    local mytextclock = wibox.widget.textbox()
    local mytextclock_tip = awful.tooltip({ objects = { mytextclock }})

    mytextclock:set_text(" | TIME | ")

    -- update the date every second
    local mytextclock_timer = gears.timer({ timeout = 1 })

    mytextclock_timer:connect_signal("timeout", utils.pcall_naughty_timer(mytextclock_timer, function()
        local result = platform.time.date()
        utils.result_async(result, function(stdout)
            local text = platform.time.date_callback(stdout)
            mytextclock:set_text("| " .. text)
        end)
    end))

    -- only update the calendar when mouse enters
    mytextclock:connect_signal("mouse::enter",
        utils.pcall_naughty(function()
            local calendar_result = platform.time.calendar()
            local date_result = platform.time.calendar_current_date()

            utils.result_async(calendar_result, function(calendar_stdout)
                utils.result_async(date_result, function(date_stdout)
                    local day = platform.time.calendar_extract_day(date_stdout)
                    local markup_out = platform.time.calendar_markup(calendar_stdout, day)
                    mytextclock_tip.markup = '<span font="Iosevka 9">' .. markup_out .. '</span>'
                end)
            end)
        end)
    )

    mytextclock_timer:start()

    return {mytextclock}
end

return mod
