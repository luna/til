local mod = {}

local wibox = require("wibox")
local awful = require("awful")
local utils = require("utils")

local platform = require("platform")

local icon_dir = platform.config.volume_mate_icon_dir or "/usr/share/icons/mate/24x24/status/"

local function update_widget(widget, tip, data)
    if data.volume == nil then
        widget:set_image(icon_dir .. "audio-volume-muted.png")
        tip:set_text("[Muted]")
    elseif data.volume == -1 then
        widget:set_image(icon_dir .. "audio-volume-off.png")
        tip:set_text("[error]")
    else
        if data.volume >= 66 then
            widget:set_image(icon_dir .. "audio-volume-high.png")
        elseif data.volume >= 33 then
            widget:set_image(icon_dir .. "audio-volume-medium.png")
        else
            widget:set_image(icon_dir .. "audio-volume-low.png")
        end

        local channel = platform.sound.mixer_channel()
        tip:set_text(channel .. ": " .. data.volume .. "%")
    end
end


function mod.CreateWidget()
    assert(platform.config.terminal, "config key 'terminal' required")
    assert(platform.sound.mixer_state, "platform does not provide mixer state")
    assert(platform.sound.mixer_state_callback, "platform does not provide mixer state callback")

    local volume_widget = wibox.widget.imagebox()
    local volume_widget_tip = awful.tooltip({ objects = { volume_widget }})

    local function execute(platform_function)
        local result = platform_function()
        utils.result_async(result, function(exitcode, stdout)
            local data = platform.sound.mixer_state_callback_v2(exitcode, stdout)
            update_widget(volume_widget, volume_widget_tip, data)
        end, { errors = 1 })
    end

    -- update widget data on first run
    execute(platform.sound.mixer_state)

    -- assumes sound is given by a command for now.
    volume_widget:buttons(awful.util.table.join(
        awful.button({}, 1, function()
            execute(platform.sound.toggle_mute)
        end),
        awful.button({}, 4, function()
            execute(platform.sound.volume_up)
        end),
        awful.button({}, 5, function()
            execute(platform.sound.volume_down)
        end),
        awful.button({}, 3, function()
            awful.spawn(platform.config.terminal .. " -e " .. platform.sound.mixer_command())
        end)
    ))

    -- always update widget when mouse enters widget
    volume_widget:connect_signal("mouse::enter", function()
        execute(platform.sound.mixer_state)
    end)

    return {volume_widget}
end

return mod
