local mod = {}

local config = require("config")

local json = require("json")

local function split_newlines(str)
    return str:gmatch("[^\r\n]+")
end

function mod.battery()
    local battery_device = config["battery"]
    assert(battery_device, "battery_device must not be nil")

    return {type='multiple', futures={
        -- charge info
        {type='file', filepath="/sys/class/power_supply/"..battery_device.."/charge_now"},
        {type='file', filepath="/sys/class/power_supply/"..battery_device.."/charge_full"},
        -- tip text
        {type='cmd', command="acpi"}
    }}
end

function mod.battery_callback(data)
    local cur = data[1]
    local cap = data[2]
    assert(cur, "no battery charge current")
    assert(cur, "no battery charge cap")

    local battery = math.floor(cur * 100 / cap)

    local result = {}

    result.tip = data[3]
    result.percentage = tostring(battery)

    return result
end

function mod.ram_usage()
    return {type='file', filepath='/proc/meminfo'}
end

function mod.ram_usage_callback(stdout)
    local available, total

    for line in split_newlines(stdout) do
        for key, value in string.gmatch(line, "(%w+): +(%d+).+") do
            if key == "MemAvailable" then
                available = tonumber(value)
            elseif key == "MemTotal" then
                total = tonumber(value)
            end
        end
    end

    assert(available, "MemAvailable not found in meminfo")
    assert(total, "MemTotal not found in meminfo")

    return 100 - ((available / total) * 100)
end

function mod.cpu_usage()
    return {type='file', filepath='/proc/stat'}
end

local jiffies = {}
function mod.cpu_usage_callback(stdout)
    local s = ""
    local totalcpu = 0
    local totalwork = 0
    local finalwork
    for line in split_newlines(stdout) do
        local cpu, newjiffies = string.match(line, "(cpu%d*) +(%d+)")
        if cpu and newjiffies then
            if not jiffies[cpu] then
                jiffies[cpu] = newjiffies
            end

            -- calculate total average
            totalcpu = totalcpu + 1
            totalwork = totalwork + (newjiffies - jiffies[cpu])

            finalwork = totalwork / totalcpu

            jiffies[cpu] = newjiffies
        end
    end

    assert(finalwork, "failed to calc cpu usage")
    return finalwork
end

function mod.gpu_usage()
    local gpustat_path = config.gpustat_path or 'gpustat'
    return {type='cmd', command=gpustat_path .. ' --json'}
end

function mod.gpu_usage_callback(stdout)
    assert(stdout)
    local data = json.decode(stdout)

    local first_gpu = data["gpus"][1]
    if first_gpu == nil then
        return 0
    end

    local usage = first_gpu["utilization.gpu"]
    assert(usage, "failed to get gpu usage")
    return usage
end

local sound = {}

function sound.mixer_channel()
    return config.mixer_channel or "Master"
end

function sound.mixer_command()
    return config.mixer or "alsamixer"
end

local pactl_state_command = 'pactl get-sink-volume @DEFAULT_SINK@ && pactl get-sink-mute @DEFAULT_SINK@'

function sound.mixer_state()
    return {type='shell', command=pactl_state_command}
end

function sound.toggle_mute()
    return {type='shell', command='pactl set-sink-mute @DEFAULT_SINK@ toggle && '..pactl_state_command}
end

function sound.volume_up()
    return {type='shell', command='pactl set-sink-volume @DEFAULT_SINK@ +5% && '..pactl_state_command}
end

function sound.volume_down()
    return {type='shell', command='pactl set-sink-volume @DEFAULT_SINK@ -5% && '..pactl_state_command}
end

function sound.mixer_state_callback(stdout)
    local mute = string.match(stdout, 'Mute: (%a+)')
    local volume = string.match(stdout, "(%d+)%%")
    assert(volume, "volume should not be nil. out=" .. stdout)
    assert(mute, "mute should not be nil. out=" .. stdout)

    if mute == "yes" then
        return {volume=nil}
    else
        return {volume=tonumber(volume)}
    end
end

function sound.mixer_state_callback_v2(exitcode, stdout)
    if exitcode ~= 0 then
        return {volume=-1}
    end

    local mute = string.match(stdout, 'Mute: (%a+)')
    local volume = string.match(stdout, "(%d+)%%")
    assert(volume, "volume should not be nil. out=" .. stdout)
    assert(mute, "mute should not be nil. out=" .. stdout)

    if mute == "yes" then
        return {volume=nil}
    else
        return {volume=tonumber(volume)}
    end
end

mod.sound = sound

local time = {}
function time.date()
    return {type='cmd', command="date"}
end

function time.date_callback(stdout)
    return string.sub(stdout, 0, -10)
end

function time.calendar()
    return {type='cmd', command="cal"}
end

function time.calendar_current_date()
    return {type='cmd', command="date -Idate"}
end

function time.calendar_extract_day(stdout)
    local _, _, day = stdout:match('(%d+)-(%d+)-(%d+)')
    assert(day ~= nil, "day not found. " .. stdout)
    return day
end

function time.calendar_markup(stdout, day)
    -- normalize `cal` output by making all numbers given by it
    -- be formatted as two digits (since date -Idate gives two digits, and we
    -- need to format it well)
    local normalized = stdout:gsub("  (%d)", " 0%1")
    normalized = normalized:gsub(" (%d) ", "0%1 ")

    return normalized:gsub(day, "<b><i>%1</i></b>")
end

mod.time = time

local network = {}

function network.wifi_strength()
    return {type='cmd', command="awk 'NR==3 {printf \"%.1f%%\\n\",($3/70)*100}' /proc/net/wireless"}
end

function network.wifi_strength_callback(stdout)
    if stdout == "" then
        return {strength=nil}
    else
        return {strength=tostring(stdout)}
    end
end

local function _net_state_work_iface(state, interface, line)
    -- Received bytes, first value after the interface
    local recv = tonumber(string.match(line, ":[%s]*([%d]+)"))
    -- Transmited bytes, 7 fields from end of the line
    local send = tonumber(string.match(line,
     "([%d]+)%s+%d+%s+%d+%s+%d+%s+%d+%s+%d+%s+%d+%s+%d$"))

    local now = os.time()

    if state[interface] == nil then
        -- Default values on the first run
        state[interface] = {}
    else
        -- Net stats are absolute, substract our last reading
        local interval = now - state[interface].time
        if interval <= 0 then interval = 1 end

        local down = (recv - state[interface][1]) / interval
        local up   = (send - state[interface][2]) / interval

        state[interface].down = down
        state[interface].up = up
    end

    state[interface].time = now

    -- Store totals
    state[interface][1] = recv
    state[interface][2] = send
end

function network.state_tick(state)
    for line in io.lines("/proc/net/dev") do
        -- Match wmaster0 as well as rt0 (multiple leading spaces)
        local iface = string.match(line, "^[%s]?[%s]?[%s]?[%s]?([%w]+):")
        if iface ~= nil then
            _net_state_work_iface(state, iface, line)
        end
    end
end

mod.network = network

local temperatures = {}

function temperatures.fetch_all()
    return {type='cmd', command="sensors -j"}
end

function temperatures.fetch_all_callback(stdout)
    assert(stdout)
    local data = json.decode(stdout)
    local result = {}
    for adapter, sensors in pairs(data) do
        result[adapter] = {}
        for property_key, property_value in pairs(sensors) do
            if property_key ~= "Adapter" then
                for temperature_key, temperature_value in pairs(property_value) do
                    for _ in temperature_key:gmatch("(%S+)_input") do
                        result[adapter][property_key] = tonumber(temperature_value)
                    end
                end
            end
        end
    end

    return result
end

mod.temperatures = temperatures

return mod
