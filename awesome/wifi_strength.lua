local mod = {}

local awful = require("awful")
local wibox = require("wibox")
local gears = require("gears")
local platform = require("platform")
local utils = require("utils")

local function update_widget(widget, tip, result)
    if result.strength == nil then
        widget:set_text(" Wdown |")
        tip:set_text("Wifi is down!")
    else
        widget:set_text("w" .. result.strength .. " |")
        tip:set_text("wifi strength: " .. result.strength)
    end
end

function mod.CreateWidget()
    local wifi_signal_widget = wibox.widget.textbox(" w?% |")
    local wifi_signal_widget_tip = awful.tooltip({ objects = { wifi_signal_widget }})

    local wifi_timer = gears.timer({ timeout = 10 })
    wifi_timer:connect_signal("timeout", function()
        local result = platform.network.wifi_strength()
        utils.result_async(result, function(stdout)
            local strength = platform.network.wifi_strength_callback(stdout)
            update_widget(wifi_signal_widget, wifi_signal_widget_tip, strength)
        end)
    end)
    wifi_timer:start()

    return {wifi_signal_widget}
end

return mod
