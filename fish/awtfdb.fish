function __tag_complete
    # Get the entire command line up to cursor
    set -l cmd (commandline -cop)
    # Get current token
    set -l current_token (commandline -t)

    # Find if we're after a -t flag
    if string match -q -- "*-t" "$cmd"
        # We're after -t, so this token is our search term
        /home/luna/git/awtfdb/extra/completion.py $current_token
        return
    end

    # If we're not after -t, return without completions
    return 1
end

function __any_tag_complete
    # Get the entire command line up to cursor
    set -l cmd (commandline -cop)
    # Get current token
    set -l current_token (commandline -t)

    /home/luna/git/awtfdb/extra/completion.py $current_token
end

complete -e ainclude
complete -e afind

# Add tag completion when we're after a -t flag
complete -c ainclude -k -n __tag_complete -a '(__tag_complete)'
complete -c afind -k -n __tag_complete -a '(__any_tag_complete)'
# Allow normal file completion
complete -c ainclude -F
