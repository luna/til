function fish_greeting --description 'awooga'
    if [ "$TIL_FISH_GREETING" = "0" ];
        return
    end
    set should_run (date +%s%3N | gawk '{
    srand($1);
    selected_probability = rand() * 100;
    if(selected_probability < 10) {
        print "1"
    }
}')
    if [ "$should_run" = "1" ]
        fortune -e /home/luna/elixire-fortune/catgirl-starboard.fortune /home/luna/elixire-fortune/elixire-starboard.fortune /home/luna/elixire-quotes.fortune /home/luna/elixire-fortune/petoria-2022-02.fortune
    end
end
