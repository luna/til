# fish_git_prompt configuration, taken of kawasaki theme
set __fish_git_prompt_color_merging              red
set __fish_git_prompt_color_branch               brblue
set __fish_git_prompt_showcolorhints             yes
set __fish_git_prompt_show_informative_status    yes
set __fish_git_prompt_char_stateseparator        ' '

# enforce non-unicode prompt chars
set __fish_git_prompt_char_cleanstate '✔'
set __fish_git_prompt_char_dirtystate '*'
set __fish_git_prompt_char_invalidstate '#'
set __fish_git_prompt_char_stagedstate '+'
set __fish_git_prompt_char_stashstate '$'
set __fish_git_prompt_char_untrackedfiles '%'
set __fish_git_prompt_char_upstream_ahead '>'
set __fish_git_prompt_char_upstream_behind '<'
set __fish_git_prompt_char_upstream_diverged '<>'
set __fish_git_prompt_char_upstream_equal '='
set __fish_git_prompt_char_upstream_prefix ''


function __my_prompt_git
    # the sed call was also taken off kawasaki, my modifications were
    # simplification of code by using set_color
    set git_prompt (set_color red)(__fish_git_prompt | command sed -e 's/^ (//' -e 's/)$//')
    echo $git_prompt
end

function color_from_hostname --description 'generate hex color from hostname'
    gawk --non-decimal-data -v input_data=$argv[1] '
    function max(a, b) {
        if (a > b) return a;
        return b;
    }
    function min(a, b) {
        if (a > b) return b;
        return a;
    }

    function with_component(component_hex) {
        component_num = sprintf("%d", "0x" component_hex);
        return and(max(0xaa, 0xaa + (component_num * 3)), 0xff);
    }

    function calculate_crc32(data) {
        cmd = sprintf("echo \\"%s\\" | cksum ",data);
        while ( (cmd | getline cksum) > 0) {
            split(cksum, splitted, " ");
            cksum_result_numeric = splitted[1];
            cksum_result_hex = sprintf("%x", cksum_result_numeric);
        }
        close(cmd);
        return cksum_result_hex;
    }

    # magic from
    # https://stackoverflow.com/questions/9733288/how-to-programmatically-calculate-the-contrast-ratio-between-two-colors

    function srgb_luminance(r, g, b) {
        vec[1] = r;
        vec[2] = g;
        vec[3] = b;
        for (idx in vec) {
            color = vec[idx];
            color = color / 255;
            if (color < 0.03928) {
                out_color = color / 12.92;
            } else {
                out_color = ((color + 0.055) / 1.055) ** 2.4;
            }
            out_vec[idx] = out_color
            idx++;
        }

        return out_vec[1] * 0.2126 + out_vec[2] * 0.7152 + out_vec[3] * 0.0722;
    }

    function srgb_contrast(r1, g1, b1) {
        given_color_luminance = srgb_luminance(r1, g1, b1);
        # my terminals background is hardcoded here
        background_luminance = srgb_luminance(0x1d, 0x1f, 0x28);
        brightest = max(given_color_luminance, background_luminance);
        darkest = min(given_color_luminance, background_luminance);
        return (brightest + 0.05) / (darkest + 0.05);
    }

    function rgb_inverse(r, g, b, out) {
        out[1] = 255 - r;
        out[2] = 255 - g;
        out[3] = 255 - b;
    }

    BEGIN {
        input = calculate_crc32(input_data);
        r_component_from_crc = substr(input, 0, 2);
        g_component_from_crc = substr(input, 3, 2);
        b_component_from_crc = substr(input, 5, 2);

        r_component = with_component(r_component_from_crc);
        g_component = with_component(g_component_from_crc);
        b_component = with_component(b_component_from_crc);

        contrast = srgb_contrast(r_component, g_component, b_component);
        if (contrast < 4.5) {
            rgb_inverse(r_component,g_component,b_component, outvec);
            r_component = outvec[1];
            g_component = outvec[2];
            b_component = outvec[3];
        }

        printf("%02x%02x%02x", r_component, g_component, b_component);

    }
    '
end

function fish_prompt --description 'Write out the prompt (luna style)'
    # the default changes depending if root or not, but
    # those configs are linked to ~/.config/fish for my user, not
    # root, so i can just hardcode those as they are. extract milliseconds,
    # as they say..
    set color_cwd $fish_color_cwd

    # split the main prompt into its components
    set userhost "$USER@"(prompt_hostname)

    set cwd ""(set_color $color_cwd)(prompt_pwd)
    set git (__my_prompt_git)
    set suffix (set_color normal)'$ '
    if set -q VIRTUAL_ENV
        set vf (echo -n -s (set_color -b magenta white) "(vf: " (basename "$VIRTUAL_ENV") ")" (set_color normal) " ")
    else
        set vf (echo)
    end

    set pretty_hostname (set_color (color_from_hostname $hostname))$hostname(set_color normal)

    # join them together
    printf "%s@%s:%s %s %s\n%s" $USER $pretty_hostname $cwd $git $vf $suffix 
end
