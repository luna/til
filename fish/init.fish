#!/usr/bin/fish

# load custom env vars on home dir
function setenv
    set -gx $argv
end

if [ -e "$HOME/.til_env" ]
    source "$HOME/.til_env"
else
    echo "warn: .til_env should exist"
end

# load most of my stuff
source "$TIL_HOME/fish/path.fish"
source "$TIL_HOME/fish/alias.fish"

# keep features separated by env vars
if [ $TIL_SSH_AGENT = 1 ]
    source "$TIL_HOME/fish/ssh-agent.fish"
end

if [ $TIL_VIRTUALFISH = 1 ]
    source "$TIL_HOME/fish/virtualfish.fish"
end

if [ $TIL_AWTFDB_COMPLETIONS = 1 ]
    source "$TIL_HOME/fish/awtfdb.fish"
end

set hook_data (direnv hook fish)
echo $hook_data | source -

if [ -e "$HOME/.config/til/custom.fish" ]
    source "$HOME/.config/til/custom.fish"
end
