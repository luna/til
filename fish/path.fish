#!/usr/bin/fish

# some product
set PATH $HOME/.radicle/bin $PATH

# my stuff
set PATH $HOME/.local/bin $PATH
set PATH $HOME/til/bin $PATH
set PATH $HOME/git/cute-scripts/bin $PATH

# lang-specific
set PATH $HOME/go/bin $PATH
set PATH $HOME/.cargo/bin $PATH
set PATH $HOME/.npm-global/bin $PATH
set PATH $HOME/.luarocks/bin $PATH
set PATH $HOME/.poetry/bin $PATH
set PATH $HOME/.yarn/bin $PATH

# my binaries should be prioritized over anything else
set PATH $HOME/bin $PATH

# well these are the preview a/b system for awtfdb so these are even higher
set PATH $HOME/.local/bin-awtfdb-a/bin $PATH
