# virtualfish is a virtualenv manager that integrates with the fish shell
#
# update this when upgrading virtualfish
set -g VIRTUALFISH_VERSION 2.5.1
set -g VIRTUALFISH_PYTHON_EXEC /usr/bin/python3

# if you want to add a new plugin to it, you HAVE to source it manually,
# doing it via the vf cli will crash
source "$HOME/.local/lib/python3.9/site-packages/virtualfish/virtual.fish"
source "$HOME/.local/lib/python3.9/site-packages/virtualfish/auto_activation.fish"
source "$HOME/.local/lib/python3.9/site-packages/virtualfish/global_requirements.fish"

# signal vf that we have all plugins
emit virtualfish_did_setup_plugins
