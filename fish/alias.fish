#!/usr/bin/fish
#
# my aliases

# backup my machine using duplicity
alias moonbkp_duplicity="duplicity /home/luna file:///run/media/luna/KITTY_PALACE/moonbackup --progress --exclude \"**cache/**\" --exclude \"/home/luna/.local/share/Steam\" --exclude \"/home/luna/Virtualbox VMs/\" --exclude \"/home/luna/.cache\" --exclude \"/home/luna/.wine\" --exclude \"/home/luna/.thumbnails\" --exclude \"/home/luna/virt-machines\" "

# backup my machine using borg
alias moonbkp_borg="borg create --stats --progress --compression lz4 --exclude \"*cache/*\" --exclude \"*.local/share/Steam\" --exclude \"*/Virtualbox VMs/\" --exclude \"*/.cache\" --exclude \"*/.wine\" --exclude \"*/.thumbnails\" --exclude \"*/.local/share/Trash\" --exclude \"*/Downloads/isos/\" --exclude \"*/Downloads/iso/\" --exclude \"*.config/itch\" '::{user}-{now}' ~"

# less letters = happier me
alias e="hx"
alias p="ps aux | grep -i"
alias c+x="chmod +x -v"
alias l="ls -la"

# youtube-dl aliases
alias ytdl="yt-dlp --socket-timeout 30"
alias ytdl360="ytdl -f 'bestvideo[height<=360]+bestaudio'"
alias ytdl480="ytdl -f 'bestvideo[height<=480]+bestaudio'"
alias ytdl720="ytdl -f 'bestvideo[height<=720]+bestaudio'"
alias ytdl1080="ytdl -f 'bestvideo[height<=1080]+bestaudio'"
alias ytdl1440="ytdl -f 'bestvideo[height<=1440]+bestaudio'"

# git aliases
alias gct='git commit'
alias gctp='git commit -p'
alias gst='git status'
alias gpsh='git push'
alias gpll='git pull'
alias gact='git add -A && git commit'
alias gck='git checkout'
alias gckb='git checkout -b'
alias gbr='git branch'
alias grmt='git remote -v'
alias gmr='git merge'
alias grb='git rebase'
alias gap='git add -p'
alias gpshbb='gpsh -u origin (git_cur_branch)'
alias gpllad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} pull origin master \;'
alias gpshad='find . -mindepth 1 -maxdepth 1 -type d -exec git --git-dir={}/.git --work-tree=$PWD/{} push origin master \;'
alias shortclone="git clone --depth=1 --single-branch --no-tags"

alias git_cur_branch='git branch | grep \* | cut -d \' \' -f2'

alias findvideos='find . -iname "*mp4" -or -iname "*webm" -or -iname "*mkv" -or -iname "*avi"'

alias dwn='aria2c'
alias howmuchtodo='git grep -niE "(FIXME|TODO)" | wc -l'
alias gitdo='git grep -niE "(FIXME|TODO)" | shuf -n1'
alias plc='sudo plocate -d /var/lib/plocate/plocate.db'
alias ainc='ainclude --v1 --strict --infer-tags mime --audio-tag audio --image-tag image --video-tag video'
alias copilot='gh copilot explain'
alias soundcloud-dl='ytdl --embed-metadata --embed-thumbnail'
