set AGENT_FILE "/tmp/.til_ssh_agent"

if [ -e "$AGENT_FILE" ]
    source "$AGENT_FILE"
else
    # create ssh agent, spit out its env vars into AGENT_FILE

    if test -z (pgrep ssh-agent)
        # spawn ssh-agent, spit its vars to agent file, also run agent file
        eval (ssh-agent -c)

        # first echo truncates it so we always overwrite
        echo "set -Ux SSH_AUTH_SOCK $SSH_AUTH_SOCK" > $AGENT_FILE
        echo "set -Ux SSH_AGENT_PID $SSH_AGENT_PID" >> $AGENT_FILE

        source $AGENT_FILE
        echo (set_color blue)"ssh-agent injection: spawned new ssh-agent"
    else
        # an ssh-agent file already exists but it isn't registered
        # on $AGENT_FILE.

        echo (set_color yellow)"ssh-agent injection failed: ssh-agent already exists but it is not on $AGENT_FILE"
    end
end
