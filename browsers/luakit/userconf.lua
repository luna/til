local select = require "select"
local downloads = require "downloads"
local settings = require "settings"
local modes = require "modes"
local uri = require "lousy.uri"
local follow = require "follow"

local engines = settings.window.search_engines

engines.default = "https://duckduckgo.com/?q=%s"
downloads.default_dir = os.getenv("HOME") .. "/Downloads"

select.label_maker = function ()
    local chars = interleave("qwertasdfgzxcv12345", "yuiophjklbnm67890")
    --return sort(reverse(chars))
    return chars
end

follow.pattern_maker = follow.pattern_styles.match_label

-- load my things
require "uri2app"

modes.add_binds('normal', {
    { "h", "rewrite url to https", function(w)
        local current_uri = w.view_uri

        if current_uri == nil then
            w:notify("uri is invalid for https rewrite")
            return
        end

        local rewritten = string.gsub(current_uri, "http://", "https://")
        w:navigate(rewritten)
    end},
    { "<Control-c>", "Copy selected text.", function ()
        luakit.selection.clipboard = luakit.selection.primary
    end}
})

modes.add_binds("normal", {
    { "<Control-s>", "Stop loading the current tab.", function(w)
        w.view:stop()
    end}
})
