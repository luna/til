-- uri2app.lua
--  a simple script that calls something else when luakit opens a
--  given uri that matches a given pattern

local window = require "window"
local webview = require "webview"

-- element 1 is the matches for the incoming uri
-- element 2 is the string to launch the uri with
-- element 3 is what to notify on luakit's status bar
URL_MATCHES = {
    {
        {
            "^http://youtube.com/watch",
            "^http://www.youtube.com/watch",

            "^https://youtube.com/watch",
            "^https://www.youtube.com/watch",
        },
        "mpv %s --geometry=40%%"
        ,
        "launched youtube url ('%s') in mpv"
    },

    {
        { "^magnet:", },
        "qbittorrent %s",
        "launched qbittorrent for magnet url"
    },

    {
        { "^https://twitch.tv/%a+" },
        "streamlink %s best",
        "launched streamlink for '%s'"
    }
}


webview.add_signal("init", function (view)
    view:add_signal("navigation-request", function (v, uri_incoming)
        -- i haven't found a function that gets the window
        -- for the given view so i'm just copying straight from luakit source
        -- lmao
        w = window.ancestor(v)
        uri_lower = string.lower(uri_incoming)

        for _, elements in pairs(URL_MATCHES) do
            for _, match in pairs(elements[1]) do
                if string.match(uri_lower, match) then
                    luakit.spawn(
                        string.format(elements[2], tostring(uri_incoming))
                    )

                    w:notify("[uri2app] " ..
                        string.format(elements[3], uri_incoming))
                    return false
                end
            end
        end

    end)
end)

