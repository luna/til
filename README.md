# til

dotfiles

## setup

- create `~/.env` file

```
setenv TIL_VIRTUALFISH "1"
setenv TIL_HOME "/home/awoo/til"
```

- create `~/.config/til.conf` file

- run `symlink.sh` (and pray)

## til.conf file

the `til.conf` file basically describes the machine so that other code
may do different things.

at the moment, used by awesomewm scripts only

|     field | description                                             |
| --------: | :------------------------------------------------------ |
|  platform | which os/platform is this machine on. `"linux"` for now |
|  terminal | which terminal to use. must be in `$PATH`               |
| wallpaper | which wallpaper should be used for awesomewm            |
|     mixer | which command to use to spawn a mixer in the terminal   |

## linux platform specific fields

|             field | description                                                                    |
| ----------------: | :----------------------------------------------------------------------------- |
|           battery | which battery to be used for the battery widget. see `/sys/class/power_supply` |
| network_interface | which network interface to be used for the network widget. see `ip link`       |
