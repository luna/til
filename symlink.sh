#!/bin/sh

TIL_PREFIX=${TIL_PREFIX:-"$HOME/til"}
target=${TIL_TARGET:-"$HOME"}

custom_ln() {
    if [ -h "$2" ]; then
        echo "EXISTS '$2'"
    else
        if ln -f -s "$1" "$2"; then
            echo "OK ln '$1' -> '$2'"
        fi
    fi
}

custom_mkdir() {
    if mkdir -p "$1"; then
        echo "OK mkdir '$1'"
    fi
}

ln="custom_ln"
mkdir="custom_mkdir"

# fish
$mkdir "$target/.config/fish"
$ln "$TIL_PREFIX/fish/init.fish" "$target/.config/fish/config.fish"
$ln "$TIL_PREFIX/fish/functions" "$target/.config/fish/"

# awesome
$ln "$TIL_PREFIX/awesome" "$target/.config"
$ln "$TIL_PREFIX/config/X11/Xresources" "$target/.Xresources"

# vim and nvim
$mkdir "$target/.config/nvim"
$mkdir "$target/.local/share/nvim"
$mkdir "$target/.local/share/nvim/plugged"

vim_base_files() {
    echo "OK vim base files at prefix '$1'"
    mkdir -p "$1/info"
    mkdir -p "$1/backup"
    mkdir -p "$1/swap"
    mkdir -p "$1/undo"
}

vim_base_files "$target/.vim/files/"
vim_base_files "$target/.nvim/files/"

$ln "$TIL_PREFIX/vim/vimrc" "$target/.vimrc"
$ln "$TIL_PREFIX/vim/nvimrc" "$target/.config/nvim/init.vim"

# emacs configuration
#$mkdir ~/.emacs.d
#$ln "$TIL_PREFIX/emacs/.emacs" "$target/.emacs"
#$ln "$TIL_PREFIX/emacs/.emacs.d" "$target/.emacs.d"

# config files
$ln "$TIL_PREFIX/config/tmux.conf" "$target/.tmux.conf"

$mkdir "$target/.config/kitty"
$ln "$TIL_PREFIX/config/kitty.conf" "$target/.config/kitty/kitty.conf"

# qutebrowser userscripts
$mkdir "$target/.local/share/qutebrowser/"
$ln "$TIL_PREFIX/browsers/qutebrowser/userscripts" "$target/.local/share/qutebrowser/userscripts"

# luakit config
$ln "$TIL_PREFIX/browsers/luakit" "$target/.config/luakit"

# kak
$mkdir "$target/.config/kak"
$ln "$TIL_PREFIX/kak/kakrc" "$target/.config/kak/kakrc"

# alacritty
$mkdir "$target/.config/alacritty"
$ln "$TIL_PREFIX/config/alacritty.yml" "$target/.config/alacritty/alacritty.yml"
$ln "$TIL_PREFIX/helix" "$target/.config/helix"
$ln "$TIL_PREFIX/config/wezterm.lua" "$target/.wezterm.lua"
